<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class ImportController extends Controller
{

    private $path_upload = '/home/vagrant/Code/Laravel/uploads/';

    public function __construct(){

    }

    public function index(){

        $tables = DB::select('SHOW TABLES from homestead like "import%"');
        foreach ($tables as $table){
            $arrTable = (array)$table;
            $data['tables'][] = $arrTable['Tables_in_homestead (import%)'];
        }
        return view('import.addStep', $data);
    }

    public function post(){

        $rows = '<option></option>';

        if(!empty($_FILES)) {

            copy($_FILES['userpic']['tmp_name'],$this->path_upload.basename($_FILES['userpic']['name']));

            $fl = Excel::load($_FILES['userpic']['tmp_name'], function ($reader) {
            })->get();

            $titles = $fl->getHeading();

            foreach ($titles as $title) {
                $rows .= '<option>' . $title . '</option>';
            }

        }
        $data['rows'] = $rows;
        $data['fldir'] = $_FILES['userpic']['name'];
        return $data;
    }

    public function put(Request $request){

        $data = [];

        if($request->input('tabName', false)){
            $columns = DB::select('SHOW COLUMNS FROM ' . $request->input('tabName'));
        }

        $data['table'] = '<table>';

        foreach ($columns as $column) {
            if ($column->Extra != 'auto_increment' && $column->Field !='created_at' && $column->Field !='updated_at'){
                $column->Default == '' ? $tipe= '*' : $tipe= '';
                $column->Default == '' ? $tipeClass= ' required' : $tipeClass= '';
                $data['table'] .= '<tr><td class="columns">' .$column->Field. ''. $tipe .'</td><td><select class="rows' . $tipeClass . '" name="' .$column->Field. '"></select></td></tr>';
            }
        }

        $data['table'] .= '</table>';
        $data['tabUpload'] = $request->input('tabName', false);

        return $data;
    }

    public function importFile(Request $request){

        $rows = $request->all();

        $fldir = $rows['fldir'];
        unset($rows['fldir']);
        $tabUpload = $rows['tabUpload'];
        unset($rows['tabUpload']);
        $tabUpload = 'App\\'.$tabUpload;
        $datas = Excel::load($this->path_upload.$fldir, function ($reader) {})->get();
        $datas = $datas->toArray();
        $count = count($datas);
        $counts = 0;
        foreach ($datas as $data){
            $s = [];
            try{
                foreach ($rows as $key => $value ){
                    if(!empty($value)) {

                   $s[$key] = $data[$value];

                    }
                }
                if(true){
                    $tabUpload::create($s);
                }else{
                    $tabUpload::updateOrCreate(
                        ['name' => $s['name']],
                        $s
                    );
                }
                $counts++;
            }catch (Exception $e){
                print_r($e);
            }
        }
        $add['add'] = 'Add '.$counts.' strings with '.$count;
        return $add;
    }

}
