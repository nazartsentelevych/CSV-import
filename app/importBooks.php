<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class importBooks extends Model
{
    //

    protected $table = 'importBooks';

    protected $fillable = ['name','author','stile'];
}
