<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('importBooks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('author', 50);
            $table->string('stile', 50)->nullable()->default(null);
            $table->string('created_at');
            $table->string('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('importBooks', function (Blueprint $table) {
            //
        });
    }
}
