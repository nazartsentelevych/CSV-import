// $( document ).ready(function() {
//
//     $( "#AddDeny" ).on( "click", function() {
//         //console.log('AddDeny' );
//         AddEdit("add", "", "");
//     });
//
//     $( ".EditDeny" ).on( "click", function() {
//         AddEdit("edit", $(this).data( "id" ), $(this).data( "phone" ));
//     });
//
//     $( ".DeleteDeny" ).on( "click", function() {
//
//         var token = $('[name=_token]').val();
//         var id = $(this).data( "id" );
//         $.ajax({
//             type: 'delete',
//             headers: {
//                 'X-CSRF-TOKEN': token
//             },
//             url: "/cmsadmin/deny",
//             data: {
//                 id: id
//             },
//             success: function(data) {
//                 $( ".phonerow[data-id="+id+"]").remove();
//                 // alert('Phone number - '+ phone + ' deleted! ');
//                 // $("#Deny").modal('toggle');
//             }
//         });
//     });
//
//     function AddEdit(action, id, phone) {
//         $("#PhoneNum").val(phone);
//         $("#PhoneNum").data("id", id);
//         $("#PhoneNum").data("action", action);
//         if (action == 'add') {
//             $("#ButtonAction").html("Add");
//         } else {
//             $("#ButtonAction").html("Save");
//         }
//
//         $("#Deny").modal('toggle');
//
//         $( "#ButtonAction" ).on( "click", function() {
//             var action = $("#PhoneNum").data("action");
//             var id = $("#PhoneNum").data("id");
//             var phone = $("#PhoneNum").val();
//             var token = $('[name=_token]').val();
//
//             if(action == 'add'){
//                 $.ajax({
//                     type: 'post',
//                     headers: {
//                         'X-CSRF-TOKEN': token
//                     },
//                     url: "/cmsadmin/deny",
//                     data: {
//                         phone: phone
//                     },
//                     success: function(data) {
//                         alert('Phone number - '+ phone + ' added! ');
//                         $("#Deny").modal('toggle');
//                         location.reload();
//                     }
//                 });
//
//             } else {
//
//                 $.ajax({
//                     type: 'put',
//                     headers: {
//                         'X-CSRF-TOKEN': token
//                     },
//                     url: "/cmsadmin/deny",
//                     data: {
//                         phone: phone,
//                         id: id
//                     },
//                     success: function(data) {
//                         alert('Phone number - '+ phone + ' edited! ');
//                         $("#Deny").modal('toggle');
//                         $( ".phone[data-id="+id+"]").html(phone);
//                     }
//                 });
//             }
//         });
//     }
//
//
// });