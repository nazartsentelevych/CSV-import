@extends ('main')
@section('content')
    <style>
        table{
            margin: 0 auto;
            /*width: 80%;*/
            width: 600px; border-collapse: collapse;
        }
        tr{
            padding: 0;
            margin: 0;
            font-size: 24px;
            margin-bottom: 40px;
            height: 60px;
        }
        .columns{
            text-align: left;
        }
        .rows{
            /*width: 100px;*/
            border-radius: 10px;
            width: 300px;
            height: 30px;
        }
        .label{
            font-size: 24px;
            color: darkslateblue;
            margin: 100px;
            font-family: 'Raleway', sans-serif;
        }
        .write{
            border-radius: 10px;
            width: 300px;
            height: 30px;
        }
        .btn{
            border-radius: 10px;
            width: 300px;
        }
    </style>

    <input type="hidden" id="token" name="token" value="{!!csrf_token()!!}">

    <div class="content">
        <div class="title m-b-md">
            Import file!!!
        </div>
        <hr style="border: solid 1px gray">

        <form action="#" id="tab_file" enctype="multipart/form-data">
            <div class="label">
                Select a table to import data
            </div>
            <br><br>
            <div>
                <select id="tabName" name="tabName" class="write">
                    <option></option>
                    @foreach ($tables as $table)
                        <option style="color: black; font-size: 20px">{{ $table }}</option>
                    @endforeach
                </select>
            </div>
            <br>
            <div class="label">
                Select a file for import
            </div>
            <br><br>
            <div style="margin: 0px auto; text-align: left;" class="write">
                <input id="fileName" name="fileName" type="file">
            </div>
            <br>
            <div>
                <input type="button" style="" class="ok_btn btn" onClick="showTables()" value="OK">
            </div>
        </form>

        {{--<br><hr><br>--}}

        <form action="#" id="add_data" style="display: none">
            <div id="profile-client" class="setting_block">
                <div class="label">
                    Combine  table columns with file columns
                </div>
                <br><br>
            <div id="table">
            </div>
                <div>Fields marked "<span style="color: red">*</span>" are required</div>
                <input type="hidden" id="fldir" name="fldir" value="">
                <input type="hidden" id="tabUpload" name="tabUpload" value="">
                <br>
                <input type="button" class="ok_btn personal_form btn" onclick="addRegular()" value="Save">
                <input type="button" class="cencel_btn personal_form btn" onclick="closeForm()" value="Cancel">

            </div>
        </form>
    </div>
    <div id="status">
    <script>

        function showExcel() {

            var fileName = $('#fileName');
            var token = $('#token').val();
            var fd = new FormData($( "#tab_file" ));
            fd.append('userpic', fileName.prop('files')[0]);

            jQuery.ajax({
                url: "/import_file",
                type: 'POST',
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                data: fd,
                success: function (data) {
                    if(data) {
                        $(".rows").append(data.rows);
                        $("#fldir").val(data.fldir);

                        $('#tab_file').css("display", "none");
                        $('#add_data').css("display", "block");
//                            console.log('dataOk' + data);
                    }else{
//                            console.log('dataNo' + data);
                    }
                },
                error: function (data, textStatus) {
//                        console.log('dataError' + data);
//                        console.log('statusError' + textStatus);
                }
            });
        }

        function showTables(){

            var form = $("form#tab_file");

            if(form.find('#tabName').val() != '' && form.find('#fileName').val() != '' ){

                var tabName = $('#tabName').val();
                var token = $('#token').val();

                jQuery.ajax({
                    url: "/import_file",
                    type: 'put',
                    headers: {
                        'X-CSRF-TOKEN': token
                    },
                    dataType: "json",
                    data: {
                        tabName : tabName
                    },
                    success: function (data) {
                        if(data) {
//                            console.log(data.table);
                            $('#table').html(data.table);
                            $('#tabUpload').val(data.tabUpload);
                            showExcel();
//                            console.log('dataOk' + data);
                        }else{
//                            console.log('dataNo' + data);
                        }
                    },
                    error: function (data, textStatus) {
//                        console.log('dataError' + data);
//                        console.log('statusError' + textStatus);
                    }
                });
            }else{
                alert('Fill in all the fields');
            }
        }

        function addRegular() {
            var form = $("form#add_data");
            var token = $('#token').val();

            jQuery.ajax({
                url: "/import",
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: 'POST',
                data: form.serialize(),
                success: function (data) {
                    alert(data.add);
                    location.reload();
                },
                error: function (data, textStatus) {

                }
            });
        }


        function closeForm() {
            location.reload();
        }
    </script>

@stop