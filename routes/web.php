<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'ImportController@index')->name('first_step');
Route::post('/import_file', 'ImportController@post')->name('first_step_post');
Route::put('/import_file', 'ImportController@put')->name('first_step_put');

Route::post('/import', 'ImportController@importFile')->name('import_file');
